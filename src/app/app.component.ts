import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {
    var firebaseConfig = {
      apiKey: "AIzaSyD-fDSQj7YbymobOsdQeeebchWuo1NtbYE",
      authDomain: "healthprocess-e73ca.firebaseapp.com",
      databaseURL: "https://healthprocess-e73ca.firebaseio.com",
      projectId: "healthprocess-e73ca",
      storageBucket: "healthprocess-e73ca.appspot.com",
      messagingSenderId: "661619078124",
      appId: "1:661619078124:web:cb387441e1a9d3b57706c9",
      measurementId: "G-HTHD0Z9QQC"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
