import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  sign_in_form: FormGroup;
  error_message: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.init_form();
  }

  init_form() {
    this.sign_in_form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern("^[a-z0-9_-]{6,15}$")]]
    });
  }

  on_submit(){
    const email = this.sign_in_form.get('email').value;
    const password = this.sign_in_form.get('password').value;
    this.authService.sign_in_user(email, password).then(
      () => {
        this.router.navigate(['/posts']);
      },
      (error) => {
        this.error_message = error;
      }
    )
  }

}
