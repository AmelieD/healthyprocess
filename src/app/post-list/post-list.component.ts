import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { PostsService } from "../services/posts.service";
import { Post } from "../models/Post.model";
import { Router } from '@angular/router';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
  posts: Post[];
  posts_subscription: Subscription;
  constructor(private postsService: PostsService, private router: Router) { }

  ngOnInit(): void {
    this.posts_subscription = this.postsService.posts_subject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
    this.postsService.emit_posts();
    this.postsService.get_posts();
  }

  on_new_post() {
    this.router.navigate(['/posts', 'new']);
  }

  on_remove_post(post: Post) {
    this.postsService.remove_post(post);
  }

  on_show_post(id: number) {
    this.router.navigate(['/posts/' + id]);
  }

  ngOnDestroy() {
    this.posts_subscription.unsubscribe();
  }
}
