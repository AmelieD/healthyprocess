import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Post } from "../../models/Post.model";
import { Router } from "@angular/router";
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {

  post_form: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private postsService: PostsService,
              private router: Router) { }

  ngOnInit(): void {
    this.init_form();
  }

  init_form() {
    this.post_form = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  on_save_post() {
    const title = this.post_form.get('title').value;
    const description = this.post_form.get('description').value;
    const new_post = new Post(title, description);
    this.postsService.create_post(new_post);
    this.router.navigate(['/posts']);
  }


}
