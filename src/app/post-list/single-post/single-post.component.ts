import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from "../../models/Post.model";


@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {

  post: Post;

  constructor(private postsService: PostsService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    // post vide temporaire le temps de récupérer les données et éviter les erreurs
    this.post = new Post('', '');
    const id = this.route.snapshot.params['id'];
    this.postsService.get_single_post(+id).then(
      (post: Post) => {
        this.post = post;
      }
    )
  }

  on_back() {
    this.router.navigate(['/posts']);
  }

}
