import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  is_auth: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

    firebase.auth().onAuthStateChanged(
      (user) => {
        if(user) {
          this.is_auth = true;
        } else {
          this.is_auth = false;
        }
      }
    );
  }



  on_sign_out() {
    this.authService.sign_out_user();
  }

}
