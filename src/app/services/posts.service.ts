import { Injectable } from '@angular/core';
import { Post } from '../models/Post.model';
import { Subject } from "rxjs/Subject";
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  posts: Post[] = [];
  posts_subject = new Subject<Post[]>();

  constructor() { }

  emit_posts() {
    this.posts_subject.next(this.posts);
  }

  save_posts() {
    firebase.database().ref('/posts').set(this.posts);
  }

  get_posts() {
    firebase.database().ref('/posts')
      .on('value', (data) => {
        this.posts = data.val() ? data.val() : [];
        this.emit_posts();
      });
  }

  get_single_post(id: number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/posts/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  create_post(new_post: Post) {
    this.posts.push(new_post);
    console.log(new_post);
    this.save_posts();
    this.emit_posts();
  }

  remove_post(post: Post) {
    const post_index_to_remove = this.posts.findIndex(
      (post_elem) => {
        if(post_elem === post) {
          return true;
        }
      }
    );
    this.posts.splice(post_index_to_remove, 1);
    this.save_posts();
    this.emit_posts();
  }
}
